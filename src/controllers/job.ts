// import validator from 'validator';
import * as express from 'express';
import validator from 'validator';
import { IJob, IJobFilter } from '../dto';
import Job from '../models/job';
import PaymentService from '../services/payment.service';
import BroadcastService from '../services/broadcast.service';


const jobIsNotFound = (res: express.Response) => {
  res.status(404).send({
    message: 'The job is not found',
  });
};

export const getJobsList = async (
  req: express.Request,
  res: express.Response,
  next: express.NextFunction,
) => {
  // @ts-ignore
  const body: IJob = req.query;
  const filters: IJobFilter = { isActive: true };

  if (body.company && typeof body.company === 'string') {
    filters.company = body.company;
  }
  if (body.type) {
    filters.type = body.type;
  }
  if (body.category) {
    filters.category = body.category;
  }
  if (body.tags) {
    filters.tags = { $in: body.tags.split(',') };
  }

  try {
    const items = await Job.find(filters)
      .populate('company')
      .sort({ createdAt: 'desc' })
      .skip(body.page * body.limit)
      .limit(+body.limit)
      .exec();

    const count = await Job.countDocuments(filters).exec();
    res.json({
      items,
      current: +body.page,
      pages: Math.ceil(count / body.limit),
      total: count,
    });

  } catch (err) {
    return next({
      status: 400,
      errors: err,
    });
  }
};

export const addJob = async (req: express.Request, res: express.Response, next: express.NextFunction) => {
  const { title, description, tags, type, category, company, howToApply } = req.body;
  const isActive = process.env.NODE_ENV === 'test';

  try {
    const job = new Job({ title, description, tags, type, category, company, howToApply, isActive });
    const result = await job.save();
    res.send(result);
  } catch (err) {
    res.status(400).json(err.errors);
  }
};

export const jobPayment = async (req: express.Request, res: express.Response) => {
  const { paymentId, amount, jobId, email } = req.body;

  if (!jobId || !validator.isMongoId(jobId)) {
    jobIsNotFound(res);
    return;
  }

  if (!paymentId || !amount) {
    res.status(400).send(['Payment info isn\'t valid']);
  }

  try {
    const job = await Job.findById(jobId);

    if (!job) {
      jobIsNotFound(res);
      return;
    }

    const paymentService = new PaymentService();
    const payment = await paymentService.doCardPayment(paymentId, +amount, email);

    if (payment) {
      // @ts-ignore
      const result: IJob =
        await Job.findOneAndUpdate({ _id: jobId },
          { isActive: true }, { new: true })
          .populate('company');
      BroadcastService.jobCreated(result);

      res.send({
        job: result,
        payment: payment.id
      });
    } else {
      res.status(400).send(['Payment info isn\'t valid']);
    }

  } catch (err) {
    if (err.errors) {
      return res.status(400).json(err.errors);
    } else {
      return res.status(400).json(err.raw || err);
    }
  }
};

export const getJob = async (req: express.Request, res: express.Response) => {
  const { jobId } = req.params;

  try {
    if (!validator.isMongoId(jobId)) {
      jobIsNotFound(res);
      return;
    }

    const monthAgo = new Date();
    monthAgo.setMonth(monthAgo.getMonth() - 1);
    const result = await Job.find({ _id: jobId, isActive: true, createdAt: { $gte: monthAgo } })
      .populate('company');
    if (result && result[0]) {
      res.send(result[0]);
      return;
    }
    jobIsNotFound(res);

  } catch (err) {
    return res.json(err.errors);
  }
};

export const getFilters = async (req: express.Request, res: express.Response) => {
  const { keyword } = req.params;
  const startsWith = new RegExp('^' + keyword, 'i');
  try {

    const result = await Job.aggregate([
      {
        $unwind: '$tags',
      },
      {
        $match: { tags: { $regex: startsWith } },
      },
      {
        $group: {
          _id: null,
          tags: { $addToSet: '$tags' },
        },
      },
    ]);

    if (result[0] && result[0].tags) {
      res.send(result[0].tags);
    } else {
      res.send([]);
    }
  } catch (err) {
    return res.status(400).json(err.errors);
  }
};

export const removeExpiredJobs = async () => {
  const monthAgo = new Date();
  monthAgo.setMonth(monthAgo.getMonth() - 1);

  console.debug('removeExpiredJobs');
  try {
    await Job.deleteMany({ createdAt: { $lt: monthAgo } });
  } catch (err) {
    console.debug('removeExpiredJobs', err);
  }
};
