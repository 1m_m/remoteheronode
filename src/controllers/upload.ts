const { google } = require('googleapis');
import * as express from 'express';
import { Readable } from 'stream';
import config from '../config';

import { GoogleDriveAuth } from '../services/file-upload.service';
const environment = process.env.NODE_ENV;
const stage = config[environment];

export const uploadPhoto = async (req: express.Request, res: express.Response) => {

  const uploadFile = async (auth: any) => {
    const drive = google.drive({ version: 'v3', auth });
    const { originalname, mimetype, buffer } = req.file;
    const fileMetadata = {
      'name': originalname.replace(/ /g, '_'),
      parents: stage.PARENT_DRIVE_FOLDER,
    };
    const stream = new Readable();
    stream.push(buffer);
    stream.push(null);
    const media = {
      mimeType: mimetype,
      body: stream,
    };

    const file = await drive.files.create({
      resource: fileMetadata,
      media,
      uploadType: 'multipart',
      fields: 'webContentLink',
    });

    const index = file.data.webContentLink.lastIndexOf('export');
    return res
      .status(200)
      .json({
        message: 'Upload was successful',
        data: file.data.webContentLink.slice(0, index - 1),
      });
  };

  new GoogleDriveAuth(uploadFile);

};
