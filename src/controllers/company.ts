import * as express from 'express';
import validator from 'validator';
import { ICompany } from '../dto';
import Company from '../models/company';

export const addCompany = async (req: express.Request, res: express.Response) => {
  const { email, logo, name, description, website, mission } = req.body;
  const validationErrors: ICompany = {};

  if (!email || !validator.isEmail(email)) {
    validationErrors.email = 'Please enter a valid email address.';
  }

  if (website && !validator.isURL(website)) {
    validationErrors.website = 'Please enter a valid website address.';
  }

  if (logo && !validator.isURL(logo)) {
    validationErrors.logo = 'Please upload a valid logo.';
  }

  if (Object.keys(validationErrors).length) {
    return res.status(400)
      .json(validationErrors);
  }

  const companyModel = new Company({ email, name, description, website, mission, logo });
  try {
    const result = await companyModel.save();
    res.send(result);
  } catch (err) {
    return res.status(400)
      .json(err.errors);
  }
};

export const getCompanyByEmail = async (req: express.Request, res: express.Response) => {
  const { email } = req.params;
  const validationErrors: ICompany = {};
  if (!email || !validator.isEmail(email)) {
    validationErrors.email = 'Please enter a valid email address.';
  }

  if (Object.keys(validationErrors).length) {
    return res.status(400)
      .json(validationErrors);
  }

  try {
    const result = await Company.findOne({ email });

    if (!result) {
      return res.status(404)
        .json({
          message: 'Company with the email doesn\'t exist',
        });
    }
    res.send(result);
  } catch (err) {
    return res.status(400)
      .json(err.errors);
  }
};


export const getAllCompanies = async (req: express.Request, res: express.Response) => {

  try {
    const result = await Company.find({});

    res.send(result);
  } catch (err) {
    return res.status(400)
      .json(err.errors);
  }
};

export const updateCompany = async (req: express.Request, res: express.Response) => {
  const { id } = req.params;
  const { logo, name, description, website, mission } = req.body;
  const validationErrors: ICompany = {};

  if (website && !validator.isURL(website)) {
    validationErrors.website = 'Please enter a valid website address.';
  }

  if (logo && !validator.isURL(logo)) {
    validationErrors.logo = 'Please upload a valid logo.';
  }

  if (Object.keys(validationErrors).length) {
    return res.status(400)
      .json(validationErrors);
  }

  try {
    const result =
      await Company.findOneAndUpdate({ _id: id },
        { logo, name, description, website, mission }, { new: true });
    if (!result) {
      return res.status(404)
        .json({
          message: 'Company with the id doesn\'t exist',
        });
    }
    res.json(result);
  } catch (err) {
    return res.status(400)
      .json(err.errors);
  }

};
