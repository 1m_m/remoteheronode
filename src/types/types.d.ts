declare namespace NodeJS {
    export interface ProcessEnv {
        NODE_ENV: 'development' | 'production' | 'test';
        STRIPE_SECRET_KEY: string;
        JOB_MONTHLY_PAYMENT: string;
    }
}
