import config from '../config';

const fs = require('fs');
const readline = require('readline');
const { google } = require('googleapis');
const environment = process.env.NODE_ENV;
const stage = config[environment];

const SCOPES = [stage.DRIVE_SCOPE];
const TOKEN_PATH = 'token.json';

export class GoogleDriveAuth {

  constructor(callback: (v: any) => {}) {
    fs.readFile('credentials.json', 'utf8', (err: Error, credentials: any) => {
      if (err) console.debug('Error loading client secret file:', err);
      this.authorize(JSON.parse(credentials), callback);
    });

  }

  authorize(credentials: any, callback: (v: any) => {}) {
    const { client_secret, client_id, redirect_uris } = credentials.installed;
    const oAuth2Client = new google.auth.OAuth2(
      client_id, client_secret, redirect_uris[0]);

    // Check if we have previously stored a token.
    fs.readFile(TOKEN_PATH, 'utf8', (err: Error, token: string) => {
      if (err) return this.getAccessToken(oAuth2Client, callback);
      oAuth2Client.setCredentials(JSON.parse(token));
      callback(oAuth2Client);
    });
  }

  getAccessToken(oAuth2Client: any, callback: (v: any) => {}) {
    const authUrl = oAuth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: SCOPES,
    });
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    });
    rl.question('Enter the code from that page here: ', (code: string) => {
      rl.close();
      oAuth2Client.getToken(code, (err: any, token: any) => {
        if (err) return console.error('Error retrieving access token', err);
        oAuth2Client.setCredentials(token);
        // Store the token to disk for later program executions
        fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err: any) => {
          if (err) return console.error(err);
          console.log('Token stored to', TOKEN_PATH);
        });
        callback(oAuth2Client);
      });
    });
  }
}

