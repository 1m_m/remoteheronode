import { IJob } from '../dto';

process.env['NTBA_FIX_319'] = '1';

const TelegramBot = require('node-telegram-bot-api');
const bot = new TelegramBot('1187013294:AAG6KVwBTCRy8vFYvAifL2kLUac2YUj3Hw8', { polling: false });

export default class BroadcastService {
  static jobCreated(job: IJob) {
    const appLink = `https://remote-hero.com/job/${job._id}`;
    const message =
      `${job.company.name} has added a new job <b>${job.title}</b>\n${job.description}\n\n${appLink}`

    ;
    bot.sendMessage('@remote_hero', message, { parse_mode: 'HTML' });
  }
}
