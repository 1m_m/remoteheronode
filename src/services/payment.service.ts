const Stripe = require('stripe');

export default class PaymentService {
  stripe: any;

  constructor() {
    this.stripe = Stripe(process.env.STRIPE_SECRET_KEY);
  }

  async doCardPayment(paymentId: string, amount: number, email: string) {
    try {

      return this.stripe.paymentIntents.create({
        amount,
        currency: 'usd',
        description: 'Remote Hero - Creating of New Job',
        payment_method: paymentId,
        receipt_email: email,
      });
    } catch (err) {
      console.log(err);
    }
  }
}
