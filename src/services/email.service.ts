import nodemailer from 'nodemailer';

export default class EmailService {


  async addJobEmail(email: string, jobTitle: string, receiptLink: string) {
    const transport = nodemailer.createTransport({
      service: process.env.MAIL_HOST,
      auth: {
        user: process.env.MAIL_USERNAME,
        pass: process.env.MAIL_PASSWORD,
      },
    });
    const message = {
      from: 'Remote Hero',
      to: email,
      subject: 'Job is added successfully', // Subject line
      html: `
                <h2>Congrats! Your job ${jobTitle} has been added for 30 days</h2>
                <p>You can pay with promotion 10% for your next job</p>
                <p>You can find the receipt by <a href="${receiptLink}">url</a></p>
            `,
    };
    try {
      return transport.sendMail(message);
    } catch (err) {
      console.info('Error', err);
    }
  }
}
