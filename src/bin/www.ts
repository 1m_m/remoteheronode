import debug from 'debug';
import fs from 'fs';
import https from 'https';
import mongoose from 'mongoose';
import app from '../app';
import config from '../config';

const environment = process.env.NODE_ENV;
const stage = config[environment];
const _debug = debug('backend:server');

const port = normalizePort(stage.port);
app.set('port', port);

const credentials = {
  key: fs.readFileSync('./https/server.key'),
  cert: fs.readFileSync('./https/server.cert'),
};
const httpsServer = https.createServer(credentials, app);

httpsServer.listen(port);
httpsServer.on('error', onError);
httpsServer.on('listening', onListening);

mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useNewUrlParser', true);
mongoose.set('useUnifiedTopology', true);
mongoose.connect(stage.MONGO_CONN_URL);
mongoose.connection.on('error', () => {
  process.exit();
});

function normalizePort(val: any) {
  const port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

function onError(error: { syscall: string; code: any; }) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

function onListening() {
    const addr = httpsServer.address();
    if (typeof addr !== 'string') {
        console.debug('Listening on port', addr.port);
    }
}

export default app;
