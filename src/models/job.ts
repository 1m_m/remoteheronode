import mongoose, { Schema } from 'mongoose';

const jobSchema = new Schema({
  title: {
    type: String,
    max: 256,
    required: [true, 'Title is required'],
  },
  description: {
    type: String,
    required: [true, 'Description is required'],
  },
  howToApply: {
    type: String,
    max: 256,
    required: [true, 'How to apply is required'],
  },
  isAdvertisement: {
    type: Boolean,
    default: false,
  },
  region: String,
  tags: {
    type: Array,
    of: String,
    index: true,
  },
  company: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Company',
    required: true,
    index: true,
  },
  type: {
    type: String,
    enum: ['Contract', 'Full Time'],
    required: true,
    index: true,
  },
  category: {
    type: String,
    required: [true, 'Category is required'],
    index: true,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
  isActive: {
    type: Boolean,
    default: false,
  },
});

export default mongoose.model('Job', jobSchema, 'jobs');
