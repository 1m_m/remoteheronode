import mongoose, { Schema } from 'mongoose';

const companySchema = new Schema({
  name: {
    type: String,
    required: [true, 'Company name is required'],
    max: 256,
  },
  description: {
    type: String,
    max: 512,
  },
  email: {
    type: String,
    required: true,
    unique: true,
    max: 256,
  },
  logo: {
    type: String,
    max: 256,
  },
  website: {
    type: String,
    max: 256,
  },
  mission: {
    type: String,
    max: 512,
  },
});

export default mongoose.model('Company', companySchema, 'companies');
