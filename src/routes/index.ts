import { Express } from 'express';
import companyRoute from './company';
import jobRoute from './job';
import uploadRoute from './upload';

export default (app: Express) => {
  app.use('/company', companyRoute);
  app.use('/job', jobRoute);
  app.use('/upload', uploadRoute);
};
