import express from 'express';
import rateLimit from 'express-rate-limit';
import { addJob, getFilters, getJob, getJobsList, jobPayment } from '../controllers/job';

const router = express.Router();

const apiLimiter = rateLimit({
  windowMs: +process.env.UPLOAD_PHOTO_LIMIT_TIME,
  max: +process.env.UPLOAD_PHOTO_LIMIT_ATTEMPTS
});

/* GET users listing. */
router.get('/', getJobsList);
router.get('/filters/:keyword', getFilters);
router.get('/:jobId', getJob);
router.post('/payment', jobPayment);
router.post('/', apiLimiter, addJob);

export default router;
