import express from 'express';
import rateLimit from 'express-rate-limit';
import { uploadPhoto } from '../controllers/upload';

const router = express.Router();
const apiLimiter = rateLimit({
  windowMs: +process.env.UPLOAD_PHOTO_LIMIT_TIME,
  max: +process.env.UPLOAD_PHOTO_LIMIT_ATTEMPTS
});

router.post('/', apiLimiter, uploadPhoto);

export default router;
