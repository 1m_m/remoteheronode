import express from 'express';
import rateLimit from 'express-rate-limit';
import { addCompany, getCompanyByEmail, updateCompany } from '../controllers/company';

const router = express.Router();
const apiLimiter = rateLimit({
  windowMs: +process.env.ADD_COMPANY_LIMIT_TIME,
  max: +process.env.ADD_COMPANY_LIMIT_ATTEMPS,
});

router.post('/', apiLimiter, addCompany);
router.get('/:email', getCompanyByEmail);
router.put('/:id', updateCompany);

export default router;
