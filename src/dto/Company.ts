export interface ICompany {
  name?: string;
  email?: string;
  mission?: string;
  description?: string;
  logo?: string;
  website?: string;
}
