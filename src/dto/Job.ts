import { ICompany } from './Company';

export enum EJob {
  FULL_TIME = 'Full Time',
  CONTRACT = 'Contract',
}

export interface IJob {
  _id?: string;
  limit?: number;
  page?: number;
  company: ICompany;
  title: string;
  description: string;
  type: EJob;
  category: string;
  tags?: string;
  isActive?: boolean;
}
