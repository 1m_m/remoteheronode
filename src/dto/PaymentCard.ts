export interface IPaymentCard {
  expMonth: number;
  expYear: number;
  cvc: number;
  number: number;
}
