export interface IJobFilter {
  company?: string;
  type?: string;
  category?: string;
  tags?: any;
  isActive: boolean;
}
