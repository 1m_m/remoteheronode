import fs from 'fs';
import Company from '../models/company';
import Job from '../models/job';

export const insertData = async () => {
  try {
    const environment = process.env.NODE_ENV;
    const data = await Company.find();
    if (environment === 'development' && data.length < 2) {
      fs.readFile('company.json', 'utf8', async (_, companyContent) => {
        const companies = await Company.insertMany(JSON.parse(companyContent));

        fs.readFile('jobs.json', 'utf8', async (_, jobsContent) => {
          const jobs = JSON.parse(jobsContent);
          for (let job of jobs) {
            // @ts-ignore
            job.company = companies[0]._id;
          }
          await Job.insertMany(jobs);
        });

      });

    }
  } catch (err) {
    console.error(err);
  }

};

export default insertData;
