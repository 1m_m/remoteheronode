import bodyParser from 'body-parser';
import cors from 'cors';
import * as dotenv from 'dotenv';
import express from 'express';
import rateLimit from 'express-rate-limit';
import helmet from 'helmet';
import logger from 'morgan';

import multer from 'multer';
import cron from 'node-cron';
import { removeExpiredJobs } from './controllers/job';
import routes from './routes';

const multerMid = multer({
  storage: multer.memoryStorage(),
  limits: {
    fileSize: 5 * 1024 * 1024,
  },
});
const limiter = rateLimit({
  windowMs: 15 * 60 * 1000,
  max: 100,
});

//  apply to all requests
const app = express();
dotenv.config();

if (app.get('env') === 'development') {
  app.use(cors());
  console.warn('Cors are enabled');
}
app.use(logger('dev'));
app.use(bodyParser.json());

app.use(helmet.xssFilter());
app.use(helmet.frameguard());
app.use(limiter);
app.disable('x-powered-by');
app.use(multerMid.single('file'));
routes(app);
// app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use((req, res) => {
  res.status(404);
  res.json({ message: 'Not Found' });
});

app.use((err: any, req: express.Request, res: express.Response, next: express.NextFunction) => {
  res.locals.errors = err.errors;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  res.status(err.status || 500);
  res.json(err.errors || err.message || { message: 'ERROR' });
});
cron.schedule('0 22 * * *', removeExpiredJobs);


export default app;
