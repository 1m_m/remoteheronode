# Remote Hero

### How to install
  ```
$ npm install
  ```
### How to run in dev mode

 ```
$ npm run serve.dev
 ```
### How to run in prod mode

 ```
$ npm run serve.prod
 ```

### Tests

 ```
$ npm run test
 ```
