import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import request from 'supertest';
import app from '../src/bin/www';
import Company from '../src/models/company';
import {
  company,
  company2,
  company3,
  companyWithInvalidEmail,
  companyWithInvalidLogo,
  companyWithInvalidWebsite,
  companyWithoutName,
} from './mocks/company.mock';

chai.use(chaiHttp);
chai.should();

const currentApp = request.agent(app);
const companyRoute = '/company';
//const logoutRoute = '/auth/logout';

describe('Company', () => {
  before((done) => {
    Company.deleteMany(() => done());
  });

  it('Should add new company', (done) => {
    currentApp
      .post(companyRoute)
      .send(company)
      .end((err, res) => {
        expect(res.status).to.equal(200);
        done();
      });
  });

  it('Should add 2nd company', (done) => {
    currentApp
      .post(companyRoute)
      .send(company2)
      .end((err, res) => {
        expect(res.status).to.equal(200);
        done();
      });
  });

  it('Should validate new company without name', (done) => {
    currentApp
      .post(companyRoute)
      .send(companyWithoutName)
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it('Shouldn\'t add the same company', (done) => {
    currentApp
      .post(companyRoute)
      .send(company)
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it('Shouldn\'t add the company with invalid email', (done) => {
    currentApp
      .post(companyRoute)
      .send(companyWithInvalidEmail)
      .end((err, res) => {
        expect(res.status).to.equal(400);
        done();
      });
  });

  it('Should find the company by email', (done) => {
    currentApp
      .get(`${companyRoute}/${company2.email}`)
      .end((err, res) => {
        expect(res.status).to.equal(200);
        expect(res.body.name).to.equal(company2.name);
        done();
      });
  });

  it('Shouldn\'t find not existed company', (done) => {
    currentApp
      .get(`${companyRoute}/random@dssd.sdds`)
      .end((err, res) => {
        expect(res.status).to.equal(404);
        done();
      });
  });
  /*
      it('Shouldn\'t add the company with too big logo', (done) => {
          currentApp
              .post(`${companyRoute}`)
              .send(companyWithTooBigLogo)
              .end((err, res) => {
                  console.log(res.body);
                  expect(res.status).to.equal(400);
              //    expect(res.body.length).to.equal(1);
                  expect(res.body.should.include('File is too large.'));
                  done();
              })
      });*/

  it('Shouldn\'t add the company with invalid logo', (done) => {
    currentApp
      .post(`${companyRoute}`)
      .send(companyWithInvalidLogo)
      .end((err, res) => {
        expect(res.status).to.equal(400);
        expect(res.body.logo).to.equal('Please upload a valid logo.');
        done();
      });
  });

  it('Shouldn\'t add the company with invalid website', (done) => {
    currentApp
      .post(`${companyRoute}`)
      .send(companyWithInvalidWebsite)
      .end((err, res) => {
        expect(res.status).to.equal(400);
        expect(res.body.website).to.equal('Please enter a valid website address.');
        done();
      });
  });

  describe('Update Flow', () => {
    let companyId: string;
    before((done) => {
      const company = new Company(company3);
      company.save()
        .then((compObj) => {
          companyId = compObj._id;
          done();
        });
    });

    it('Should update the existing company', (done) => {
      currentApp
        .put(`${companyRoute}/${companyId}`)
        .send(company2)
        .end((err, res) => {
          expect(res.status).to.equal(200);
          expect(res.body.name).to.equal(company2.name);
          done();
        });
    });

    it('Should return 404 error fot incorrect id', (done) => {
      currentApp
        .put(`${companyRoute}/5f1319f9107af65165a15350`)
        .send(company2)
        .end((err, res) => {
          expect(res.status).to.equal(404);
          done();
        });
    });

    it('Shouldn\'t update email of the company', (done) => {
      currentApp
        .put(`${companyRoute}/${companyId}`)
        .send(company2)
        .end((err, res) => {
          expect(res.status).to.equal(200);
          expect(res.body.email).to.not.equal(company2.email);
          done();
        });
    });

  });
});
