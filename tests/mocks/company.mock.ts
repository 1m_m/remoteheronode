export const company = {
  name: '1 Company',
  email: 'marina.ageeva.1996@mail.ru',
  mission: 'lorem ipsum'
};

export const company2 = {
  name: '2 Company',
  email: 'test@gmail.com',
  mission: 'lorem ipsum'
};

export const company3 = {
  name: '2 Company',
  email: 'testcomp@gmail.com',
  mission: 'Some description'
};

export const companyWithInvalidEmail = {
  name: '3 Company',
  email: 'test2',
  mission: 'lorem ipsum'
};

export const companyWithInvalidLogo = {
  name: '3 Company',
  email: 'test2@gmail.com',
  mission: 'lorem ipsum',
  logo: '123'
};

export const companyWithInvalidWebsite = {
  name: '3 Company',
  email: 'test2@gmail.com',
  website: 'tdsds',
  mission: 'not to resp',
};

export const companyWithoutName = {
  email: 'test2@gmail.com',
};
