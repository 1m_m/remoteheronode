export const job1 = {
    title: 'Full Time Remote Designer',
    description: 'lorem ipsum doles. Sun Das',
    tags: ['Designer', 'fullTime'],
    type: 'Contract',
    category: 'Design',
    howToApply: 'remotehero.com'
};

export const job2 = {
    title: 'Full Time Remote Developer',
    description: 'lorem ipsum doles. Sun Das',
    tags: ['Designer', 'java'],
    type: 'Full Time',
    category: 'Developer',
    howToApply: 'remotehero.com'
};

export const job3 = {
    title: 'Full Time Remote Manager',
    description: 'lorem ipsum doles. Sun Das',
    tags: ['Manager', 'contract'],
    type: 'Contract',
    category: 'Manager',
    howToApply: 'remotehero.com'
};

export const job4 = {
    title: 'Contract Remote Developer',
    description: 'lorem ipsum doles. Sun Das',
    tags: ['developer', 'contract', 'c#', 'js'],
    type: 'Contract',
    category: 'Developer',
    howToApply: 'remotehero.com'
};

export const job5 = {
    title: 'Full Time Remote QA',
    description: 'lorem ipsum doles. Sun Das',
    tags: ['qa', 'full-time', 'remote'],
    type: 'Full Time',
    category: 'QA',
    howToApply: 'remotehero.com'
};

export const job6 = {
    title: 'Contract Remote Developer',
    description: 'lorem ipsum doles. Sun Das',
    tags: ['developer', 'contract', 'c++'],
    type: 'Contract',
    category: 'Developer',
    howToApply: 'remotehero.com'
};

export const job7 = {
    title: 'Full Time Remote Manager',
    description: 'lorem ipsum doles. Sun Das',
    tags: ['Manager', 'pm'],
    type: 'Full Time',
    category: 'Manager',
    howToApply: 'remotehero.com'
};

export const invalidJob = {
    title: 'Full Time Remote Designer',
    category: 'Design',
    tags: ['Designer', 'fullTime'],
    type: 'Contract',
    howToApply: 'remotehero.com'
};
