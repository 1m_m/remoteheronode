import chai, { expect } from 'chai';
import chaiHttp from 'chai-http';
import request from 'supertest';
import app from '../src/bin/www';
import Company from '../src/models/company';
import Job from '../src/models/job';
import { company, company2 } from './mocks/company.mock';
import { invalidJob, job1, job2, job3, job4, job5, job6, job7 } from './mocks/job.mock';

chai.use(chaiHttp);
chai.should();

const currentApp = request.agent(app);
const companyRoute = '/company';
const jobRoute = '/job';
let companyId: string,
    companyId2: string;
let jobId1: string,
    jobId2: string;

describe('Job', () => {
    before((done) => {
        Company.deleteMany(() => {
            Job.deleteMany(() => done());
        });
    });

    describe('Test job', () => {

        it('Should add new company', (done) => {
            currentApp
                .post(companyRoute)
                .send(company)
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    companyId = res.body._id;
                    done();
                })
        });

        it('Should add new company 2', (done) => {
            currentApp
                .post(companyRoute)
                .send(company2)
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    companyId2 = res.body._id;
                    done();
                })
        });

        it('Should add new job', (done) => {
            const jobBody = {company: companyId, ...job1};
            currentApp
                .post(jobRoute)
                .send(jobBody)
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    jobId1 = res.body._id;
                    done();
                })
        });

        it('Shouldn\'t add new job with invalid company id', (done) => {
            const jobBody = {company: '123', ...job1};
            currentApp
                .post(jobRoute)
                .send(jobBody)
                .end((err, res) => {
                    expect(res.status).to.equal(400);
                    done();
                })
        });

        it('Shouldn\'t add new job', (done) => {
            const jobBody = {company: companyId, ...invalidJob};
            currentApp
                .post(jobRoute)
                .send(jobBody)
                .end((err, res) => {
                    expect(res.status).to.equal(400);
                    done();
                })
        });
        it('Should return all jobs', (done) => {
            currentApp
                .get(jobRoute)
                .end((err, res) => {
                    expect(res.body.items.length).to.equal(1);
                    done();
                })
        });

    });

    describe('Test job pagination', () => {

        it('Should add new job 2', (done) => {
            const jobBody = {company: companyId, ...job2};
            currentApp
                .post(jobRoute)
                .send(jobBody)
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    jobId2 = res.body._id;
                    done();
                })
        });


        it('Should add new job 3', (done) => {
            const jobBody = {company: companyId, ...job3};
            currentApp
                .post(jobRoute)
                .send(jobBody)
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    done();
                })
        });


        it('Should add new job 4', (done) => {
            const jobBody = {company: companyId2, ...job4};
            currentApp
                .post(jobRoute)
                .send(jobBody)
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    done();
                })
        });


        it('Should add new job 5', (done) => {
            const jobBody = {company: companyId2, ...job5};
            currentApp
                .post(jobRoute)
                .send(jobBody)
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    done();
                })
        });


        it('Should add new job 6', (done) => {
            const jobBody = {company: companyId2, ...job6};
            currentApp
                .post(jobRoute)
                .send(jobBody)
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    done();
                })
        });


        it('Should add new job 7', (done) => {
            const jobBody = {company: companyId2, ...job7};
            currentApp
                .post(jobRoute)
                .send(jobBody)
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    done();
                })
        });


        it('Should return jobs with pagination on first page', (done) => {
            currentApp
                .get(`${jobRoute}?limit=2&page=0`)
                .end((err, res) => {
                    expect(res.body.items.length).to.equal(2);
                    done();
                })
        });

        it('Should check pagination on second page', (done) => {
            currentApp
                .get(`${jobRoute}?limit=1&page=1`)
                .end((err, res) => {
                    expect(res.body.items[0].title).to.equal(job6.title);
                    done();
                })
        });

        it('Should check sort', (done) => {
            currentApp
                .get(`${jobRoute}`)
                .end((err, res) => {
                    expect(res.body.items[0].title).to.equal(job7.title);
                    expect(res.body.items[2].title).to.equal(job5.title);
                    done();
                })
        });
    });

    describe('Get single Job', () => {
        it('Should return first job by id', (done) => {
            currentApp
                .get(`${jobRoute}/${jobId1}`)
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    expect(res.body._id).to.equal(jobId1);
                    done();
                })
        });

        it('Should return second job by id', (done) => {
            currentApp
                .get(`${jobRoute}/${jobId2}`)
                .end((err, res) => {
                    expect(res.status).to.equal(200);
                    expect(res.body._id).to.equal(jobId2);
                    done();
                })
        });

        it('Shouldn\'t return job by id', (done) => {
            currentApp
                .get(`${jobRoute}/${companyId}`)
                .end((err, res) => {
                    expect(res.status).to.equal(404);
                    done();
                })
        });

        it('Shouldn\'t return incorrect job by id', (done) => {
            currentApp
                .get(`${jobRoute}/1`)
                .end((err, res) => {
                    expect(res.status).to.equal(404);
                    done();
                })
        });
    });

    describe('Test job filtering', () => {
        it('Should filter by a company', (done) => {
            currentApp
                .get(`${jobRoute}?company=${companyId}`)
                .end((err, res) => {
                    expect(res.body.items.length).to.equal(3);
                    done();
                })
        });
        it('Should filter by a company 2', (done) => {
            currentApp
                .get(`${jobRoute}?company=${companyId2}`)
                .end((err, res) => {
                    expect(res.body.items.length).to.equal(4);
                    done();
                })
        });

        it('Should filter by a company and tags', (done) => {
            currentApp
                .get(`${jobRoute}?company=${companyId}&tags=contract,java`)
                .end((err, res) => {
                    expect(res.body.items.length).to.equal(2);
                    done();
                })
        });

        it('Should filter by type', (done) => {
            currentApp
                .get(`${jobRoute}?type=Contract`)
                .end((err, res) => {
                    expect(res.body.items.length).to.equal(4);
                    done();
                })
        });

        it('Should filter by category', (done) => {
            currentApp
                .get(`${jobRoute}?category=Developer`)
                .end((err, res) => {
                    expect(res.body.items.length).to.equal(3);
                    done();
                })
        });

        it('Should filter by category and tags', (done) => {
            currentApp
                .get(`${jobRoute}?tags=contract,java&category=Developer`)
                .end((err, res) => {
                    expect(res.body.items.length).to.equal(3);
                    done();
                })
        });

        it('Should return filters list by tags', (done) => {
            currentApp
                .get(`${jobRoute}/filters/c`)
                .end((err, res) => {
                    expect(res.body.length).to.equal(3);
                    done();
                })
        });

        it('Should return empty filters list', (done) => {
            currentApp
                .get(`${jobRoute}/filters/unknown`)
                .end((err, res) => {
                    expect(res.body.length).to.equal(0);
                    done();
                })
        });
    });
    /*    describe('Test jobs are removed after some time', function() {
           // this.timeout(5000);

            it('Should remove all jobs in 5 seconds', function (done) {
                this.timeout(20100);
                setTimeout(() => {
                    currentApp
                        .get(jobRoute)
                        .end((err, res) => {
                            expect(res.body.length).to.equal(0);
                            done();
                        });
                }, 20000);
            });
        });*/
});
